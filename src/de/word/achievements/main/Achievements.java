package de.word.achievements.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import de.word.achievements.mysql.CheckUser;
import de.word.achievements.mysql.MySQL;

public class Achievements extends JavaPlugin implements Listener{
	
	public HashMap<String, ItemStack> achievement = new HashMap<String, ItemStack>();
	public ArrayList<String> achi = new ArrayList<String>();
	public HashMap<String, String> besch = new HashMap<String, String>();
	
	public static Inventory inv;
	
	public static Achievements instance;
	@Override
	public void onEnable() {
		
		instance = this;
		
		loadConfig();
		
		String host = Utils.cfg.getString("Config.host");
		int port = Utils.cfg.getInt("Config.port");
		String database =  Utils.cfg.getString("Config.database");
		String username = Utils.cfg.getString("Config.username");
		String password = Utils.cfg.getString("Config.password");
	 
	    MySQL.connect(host, port, database, username, password);
	    
	    
	    
	    newConnect();
	    
	    getServer().getPluginManager().registerEvents(this, this);
	    
	    
	}
	
	public static void loadConfig() {
		
    Utils.cfg.addDefault("Config.host", "host");
    Utils.cfg.addDefault("Config.port", Integer.valueOf(3306));
    Utils.cfg.addDefault("Config.database", "Database");
    Utils.cfg.addDefault("Config.username", "Username");
    Utils.cfg.addDefault("Config.password", "Password");
   
    Utils.cfg.options().copyDefaults(true);
    
    try{
    	Utils.cfg.save(Utils.file);
    }catch (IOException e){
      e.printStackTrace();
    }	
	}
	
	public void newConnect(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			
			@Override
			public void run() {
				if(MySQL.isConnection() == false){
					
					String host = Utils.cfg.getString("Config.host");
					int port = Utils.cfg.getInt("Config.port");
					String database =  Utils.cfg.getString("Config.database");
					String username = Utils.cfg.getString("Config.username");
					String password = Utils.cfg.getString("Config.password");
				 
				    MySQL.connect(host, port, database, username, password);
			
				return;
				}
			}
		}, 20*900, 20*900);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		for(int i = 0; i < achi.size(); i++){
			String uu = achi.get(i);
			String[] na;
			String nana;
			if(uu.contains("-")){
				na = uu.split("-");
				nana = na[0] + na[1];
			}else{
				nana = uu;
			}
		MySQL.isUserExists(p.getUniqueId().toString(), new CheckUser() {
			
			@Override
			public void check(boolean exist) {
				if(!exist){
				MySQL.registerUser(p.getUniqueId().toString(), nana);
				}
			}
		}, nana);
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e){
		if(e.getInventory().getName() == "Achievements"){
			e.setCancelled(true);
		}
	}
}
