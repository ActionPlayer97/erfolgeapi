package de.word.achievements.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bukkit.entity.Player;

import de.word.achievements.main.Utils;

public class MySQL {
	
public static ExecutorService service;
	
	static{
		service =  Executors.newCachedThreadPool();
	}
	
	public static ExecutorService geteExecutorService(){
		return service;
	}

	
	public static Connection con;

	public static void connect(String host, int port, String database, String username, String password){
		try {
			
			con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database , username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isConnection(){
		try {
			return con != null && con.isValid(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static void isUserExists(final String uuid, final CheckUser user, final String Database){
		service.execute(new Runnable() {
			
			@Override
			public void run() {
				try{
					PreparedStatement stmt = con.prepareStatement("SELECT * FROM `" + Database + "` WHERE UUID ='" + uuid + "'");
					ResultSet rs = stmt.executeQuery();
					if(rs.next()){
						user.check(true);
					}else{
						user.check(false);
					}
				}catch(SQLException ex){
					ex.printStackTrace();
					user.check(false);
				}
				
			}
		});
	}
	
	public static void isUserExistsneu(final String uuid, final CheckUser user){
		service.execute(new Runnable() {
			
			@Override
			public void run() {
				try{
					PreparedStatement stmt = con.prepareStatement("SELECT * FROM " + Utils.cfg.getString("Config.Datenbankname") + " WHERE UUID ='" + uuid + "'");
					ResultSet rs = stmt.executeQuery();
					if(rs.next()){
						user.check(true);
					}else{
						user.check(false);
					}
				}catch(SQLException ex){
					ex.printStackTrace();
					user.check(false);
				}
				
			}
		});
	}
	
	public static void closeRessources(ResultSet rs, PreparedStatement st) {
		if(rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				System.err.println("Fehler beim schlie�en der Ressourcen");
			}
		}
		if(st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				System.err.println("Fehler beim schlie�en der Ressourcen");
			}
		}
	}
	
	public static void executeQuery(String query) {
		
		PreparedStatement st = null;
		try {
			st = con.prepareStatement(query);
			st.executeUpdate();
		} catch (Exception e) {
			System.err.println("Es gab einen Fehler beim senden der Querry");
			e.printStackTrace();
		}finally {
			closeRessources(null, st);
		}
	}

public static void registerUser(final String uuid, String ii){
	new Thread(new Runnable() {
		
		@Override
		public void run() {
			try{
				PreparedStatement M98K = con.prepareStatement("INSERT INTO " + ii + " (uuid, Bol)"
						+ "VALUES (?,?)");
				M98K.setString(1, uuid);
				M98K.setBoolean(2, false);
				M98K.executeUpdate();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}).start();
}

public static ResultSet Query(String qry) {
	ResultSet rs = null;
	
	try {
		Statement stm = con.createStatement();
		rs = stm.executeQuery(qry);
	} catch(Exception ex) {
		ex.printStackTrace();
	}
	
	return rs;
}

  public static void Update(String qry) {
		try {
			Statement state = con.createStatement();
			state.executeUpdate(qry);
			state.close();                                                              
		} catch(Exception ex) {
			String host;
			String database;
			String username;
			String password;
			int port;
			String m = "MySql";
			 host = Utils.cfg.getString(m + ".host");
			    port = Utils.cfg.getInt(m + ".port");
			    database =  Utils.cfg.getString(m + ".database");
			    username = Utils.cfg.getString(m + ".username");
			 
			    	password = Utils.cfg.getString(m + ".password");
			   
				connect(host,port,database,username,password);
			ex.printStackTrace();
		}
	}
		
	public static boolean hasAchievement(Player p, String achievementName){
		
		boolean hasGun = false;

		String uuid = p.getUniqueId().toString();
		try {
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM " +achievementName +" WHERE UUID ='" + uuid + "'");
		ResultSet rs = stmt.executeQuery();
		
			while(rs.next()) {
				hasGun = rs.getBoolean(2);
			}
			
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	return hasGun;
	}
	
	public static void setAchievement(Player p, String achievementName){
		Boolean bool = true;
		String uuid = p.getUniqueId().toString();
		executeQuery("UPDATE " + achievementName + " SET Bol= " + bool + " WHERE uuid ='" + uuid + "'");
		}
}

