package de.word.achievements.API;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import de.word.achievements.main.Achievements;
import de.word.achievements.mysql.MySQL;

public class AchievementsAPI {

	public static void addAchievement(String achievementName, ItemStack guiItem, String beschreibung){
		
		Achievements.instance.achievement.put(achievementName, guiItem);
		Achievements.instance.achi.add(achievementName);
		Achievements.instance.besch.put(achievementName, beschreibung);
	}
	
	public static void unlockAchievement(Player p, String achievementName){
		String nana;
		String[] na = null;
		if(achievementName.contains("-")){
			na = achievementName.split("-");
			nana = na[0] + na[1];
		}else{
			nana = achievementName;
		}
		if(!MySQL.hasAchievement(p, nana)){
			MySQL.setAchievement(p, nana);
			p.sendMessage("�6�k��������������������");
			p.sendMessage("�6Achievement get!");
			p.sendMessage("�a" + na[0] + " " + na[1]);
			p.sendMessage("�6�k��������������������");
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
		}else{
			return;
		}
	}	
	
	public static void createTable(){
		try {	    	
			Statement st = MySQL.con.createStatement();

			for(int i = 0; i < Achievements.instance.achi.size(); i++){
				String name = Achievements.instance.achi.get(i);
				String nana;
				if(name.contains("-")){
					String[] na = name.split("-");
					nana = na[0] + na[1];
				}else{
					nana = name;
				}
			st.executeUpdate("CREATE TABLE IF NOT EXISTS " + nana+"(UUID VARCHAR(40), Bol BOOLEAN)");
			}
			} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void openInv(Player p){
		 Achievements.inv = Bukkit.createInventory(null, 27, "�6Achievements");
		 
		 for(String name : Achievements.instance.achievement.keySet()){
			String nana;
			String[] na = null;
			if(name.contains("-")){
				na = name.split("-");
				nana = na[0] + na[1];
			}else{
				nana = name;
			}
				if(!MySQL.hasAchievement(p, nana)){
					ItemStack ino = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
					SkullMeta meta = (SkullMeta) ino.getItemMeta();
					meta.setOwner("MHF_Question");
					meta.setDisplayName("�c" + na[0] + " " + na[1]);
					meta.setLore(Arrays.asList("�5???"));
					ino.setItemMeta(meta);
					
					Achievements.inv.addItem(ino);
					
				}else{
					ItemStack ii = Achievements.instance.achievement.get(name);
					ItemMeta mm = ii.getItemMeta();
					mm.setDisplayName("�a" +  na[0] + " " + na[1]);
					mm.setLore(Arrays.asList(Achievements.instance.besch.get(name)));
					ii.setItemMeta(mm);
					Achievements.inv.addItem(ii);
				}
			}
		
		p.openInventory(Achievements.inv);
		p.updateInventory();
	}
	
//	try {
//    	String name = Utils.cfg.getString("Config.Datenbankname");
//		Statement st = MySQL.con.createStatement();
//		st.executeUpdate("CREATE TABLE IF NOT EXISTS " + name+ "(UUID VARCHAR(40), BOOL BOOLEAN)");
//		} catch (SQLException e) {
//		e.printStackTrace();
//	}
}
